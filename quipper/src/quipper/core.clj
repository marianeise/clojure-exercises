(ns quipper.core
  (require [clojure.tools.cli :as cli])
  (:gen-class))

(def cli-options [["-h" "--help" "Print this help info"
                   :default false :flag true]
                  ["-f" "--file" "Specify an alternate quip file."
                   :default (str
                             (System/getProperty "user.home")
                             "/.quips")]])
(defn -main
  "Deal with quips."
  [& args]
  (let [{:keys [options arguments summary errors]}
        (cli/parse-opts args cli-options)])
  (println "You'll need to implement this yourself."))
